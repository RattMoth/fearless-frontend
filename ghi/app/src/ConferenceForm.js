import React from 'react';

class ConferenceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      starts: '',
      ends: '',
      description: '',
      maxPresentations: '',
      maxAttendees: '',
      location: '',
      locations: []
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleStartChange = this.handleStartChange.bind(this);
    this.handleEndChange = this.handleEndChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleMaxPresentationChange = this.handleMaxPresentationChange.bind(this);
    this.handleMaxAttendeeChange = this.handleMaxAttendeeChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({locations: data.locations});
    }
  }

  handleNameChange(e) {
    const value = e.target.value;
    this.setState({name: value})
  }

  handleStartChange(e) {
    const value = e.target.value;
    this.setState({starts: value})
  }

  handleEndChange(e) {
    const value = e.target.value;
    this.setState({ends: value})
  }

  handleDescriptionChange(e) {
    const value = e.target.value;
    this.setState({description: value})
  }

  handleMaxPresentationChange(e) {
    const value = e.target.value;
    this.setState({maxPresentations: parseInt(value)})
  }

  handleMaxAttendeeChange(e) {
    const value = e.target.value;
    this.setState({maxAttendees: parseInt(value)})
  }

  handleLocationChange(e) {
    const value = e.target.value;
    this.setState({location: parseInt(value)})
  }

  async handleSubmit(e) {
    e.preventDefault();

    const data = {...this.state};
    data.max_presentations =data.maxPresentations;
    data.max_attendees = data.maxAttendees;
    delete data.maxPresentations;
    delete data.maxAttendees;
    delete data.locations;
    
    console.log(data);
    const locationUrl = 'http://localhost:8000/api/conferences/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },

    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);

      const cleared = {
        name: '',
        starts: '',
        ends: '',
        description: '',
        maxPresentations: '',
        maxAttendees: '',
        location: '',
      }

      this.setState(cleared);
    }
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new conference</h1>
              <form id="create-location-form" onSubmit={this.handleSubmit}>
                <div className="form-floating mb-3">
                  <input 
                    placeholder="Name"
                    required type="text" 
                    id="name" 
                    name="name" 
                    className="form-control"
                    onChange={this.handleNameChange}
                    value={this.state.name}
                  />
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    placeholder="mm
                    dd/yyy" required 
                    type="date" 
                    id="starts" 
                    name="starts" 
                    className="form-control"
                    onChange={this.handleStartChange}
                    value={this.state.starts}
                  />
                  <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                  <input 
                    placeholder="mm/dd/yyy" 
                    required 
                    type="date" 
                    id="ends" 
                    name="ends" 
                    className="form-control"
                    onChange={this.handleEndChange}
                    value={this.state.ends}
                  />
                  <label htmlFor="ends">Ends</label>
                </div>
                <div className="mb-3">
                  <label htmlFor="description">Description</label>
                  <textarea 
                    className="form-control" 
                    id="description" 
                    name="description" 
                    rows="3"
                    onChange={this.handleDescriptionChange}
                    value={this.state.description}
                  >
                  </textarea>
                </div>
                <div className="form-floating mb-3">
                  <input
                    placeholder="Maximum presentation"
                    required 
                    type="number"
                    id="max_presentations"
                    name="max_presentations"
                    className="form-control"
                    onChange={this.handleMaxPresentationChange}
                    value={this.state.maxPresentations}
                  />
                  <label htmlFor="max_presentations">Maximum presentations</label>
                </div>
                <div className="form-floating mb-3">
                  <input 
                    placeholder="Maximum attendee"
                    required 
                    type="number"
                    id="max_attendees"
                    name="max_attendees"
                    className="form-control"
                    onChange={this.handleMaxAttendeeChange}
                    value={this.state.maxAttendees}
                  />
                  <label htmlFor="max_attendees">Maximum attendees</label>
                </div>
                <div className="mb-3">
                  <select 
                    required 
                    id="location" 
                    name="location"
                    className="form-select"
                    onChange={this.handleLocationChange}
                    value={this.state.location}
                  >
                    {
                      this.state.locations.map(location => (
                        <option key={location.name} value={location.id}>
                          {location.name}
                        </option>
                      ))
                    }
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ConferenceForm;
