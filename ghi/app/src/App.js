import Nav from "./Nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendForm from "./AttendForm";

function App(props) {
  if (!props.attendees) {
    return null;
  }
  return (
    <>
    <Nav />
    <div className="container">
      <LocationForm />
      {/* <AttendeesList attendees={props.attendees} /> */}
      {/* <ConferenceForm /> */}
      {/* <AttendForm /> */}
    </div>
  </>
  );
}

export default App;
