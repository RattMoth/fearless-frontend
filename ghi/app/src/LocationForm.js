import React from 'react';

class LocationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      roomCount: '',
      city: '',
      states: []
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleRoomCountChange = this.handleRoomCountChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.handleStateChange = this.handleStateChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({states: data.states});
    }
  }

  handleNameChange(e) {
    const value = e.target.value;
    this.setState({name: value})
  }

  handleRoomCountChange(e) {
    const value = e.target.value;
    this.setState({roomCount: value})
  }

  handleCityChange(e) {
    const value = e.target.value;
    this.setState({city: value})
  }

  handleStateChange(e) {
    const value = e.target.value;
    this.setState({state: value})
  }

  async handleSubmit(e) {
    e.preventDefault();

    const data = {...this.state};
    data.room_count = data.roomCount;
    delete data.roomCount;
    delete data.states;
    
    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },

    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);

      const cleared = {
        name: '',
        roomCount: '',
        city: '',
        state: '',
      }

      this.setState(cleared);
    }
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new location</h1>
              <form id="create-location-form" onSubmit={this.handleSubmit}>
                <div className="form-floating mb-3">
                  <input 
                    placeholder="Name"
                    required type="text"
                    id="name" name="name" 
                    className="form-control"
                    onChange={this.handleNameChange}
                    value={this.state.name}
                  />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input 
                    placeholder="Room count"
                    required type="number"
                    id="room_count" 
                    name="room_count"
                    className="form-control"
                    onChange={this.handleRoomCountChange}
                    value={this.state.roomCount}
                  />
                  <label htmlFor="room_count">Room count</label>
                </div>
                <div className="form-floating mb-3">
                  <input 
                    placeholder="City"
                    required type="text"
                    id="city" name="city"
                    className="form-control"
                    onChange={this.handleCityChange}
                    value={this.state.city}
                  />
                  <label htmlFor="city">City</label>
                </div>
                <div className="mb-3">
                  <select
                    required 
                    id="state" 
                    name="state"
                    className="form-select" 
                    onChange={this.handleStateChange}
                    value={this.state.state}
                  >
                  {
                    this.state.states.map(state => (
                      <option key={state.abb} value={state.abb}>
                        {state.name}
                      </option>
                    ))
                  }
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default LocationForm;
