// substring(19) removes jwt_access_payload= from the start of 
// the cookie string
const payloadCookie = document.cookie.split('; ').find(row => row.startsWith('jwt_access_payload')).substring(19);

if (payloadCookie) {
  const decodedPayload = atob(payloadCookie);
  const payloadObj = JSON.parse(decodedPayload);
  console.log(payloadObj);

  if (payloadObj.user.perms.includes('events.add_conference')) {
    const conference_link = document.getElementById('conference-link');
    conference_link.classList.remove('d-none');
  }
  if (payloadObj.user.perms.includes('events.add_location')) {
    const location_link = document.getElementById('location-link');
    location_link.classList.remove('d-none');
  }
}
