function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
  return `
    <div class="col">
      <div class="card shadow mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          ${startDate} - ${endDate}
        </div>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.error("Response couldn't load.", response)
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();

          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const locationName = details.conference.location.name;

          const startStr = details.conference.starts;
          const startDate = new Date(startStr).toLocaleDateString();

          const endStr = details.conference.ends;
          const endDate = new Date(endStr).toLocaleDateString();

          const html = createCard(title, description, pictureUrl, startDate, endDate, locationName);
          const column = document.querySelector('.row');
          column.innerHTML += html;
        }
      }
    }
  } catch (message) {
      var alertPlaceholder = document.getElementById('liveAlertPlaceholder')

      function alert(message, type) {
        var wrapper = document.createElement('div')
        wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

        alertPlaceholder.append(wrapper)
      }

      if (message) {
        alert(message, 'warning')
      }
  }
});
